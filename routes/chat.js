const router = require('express').Router()
const { MessageMedia, Location } = require('whatsapp-web.js')
const request = require('request')

const vuri = require('valid-url')
const fs = require('fs');
const { phoneFormatter } = require('../helper/formatter');


const mediaDownloader = (url, path, callback) => {
    request.head(url, (err, res, body) => {
        request(url).pipe(fs.createWriteStream(path))
        .on('close',callback)
    })
}

const checkRegisteredNumber = async function (number) {
    const isRegistered = await client.isRegisteredUser(number);
    return isRegistered;
}

router.post('/sendmessage/:phone', async (req, res) => {
    let phone = phoneFormatter(req.params.phone);
    let message = req.body.message;

    if (phone == undefined || message == undefined) {
        res.send({status:"error",message: "Please enter valid phone and message"})
    } else {
        // await checkRegisteredNumber(phone).then((number) => {
        //      console.log(phone)            
        // }).catch((err) => {
        //     res.send({
        //         error: err,
        //         status: false,
        //         message: `This Number is Not Registered! ${phone}`
        //     });
        // });
  
        client.sendMessage(phone, message).then((resp) => {
            if (resp.id.fromMe) {
                res.send({status:'success', message: `Message Sent Successfully to ${phone}`})
            }
        })
        
    }
})

router.post('/sendImage/:phone', async (req, res) => {
    var base64regex = /^([0-9a-zA-Z+/]{4})*(([0-9a-zA-Z+/]{2}==)|([0-9a-zA-Z+/]{3}=))?$/;

    let phone = phoneFormatter(req.params.phone);
    let image = req.body.image;
    let caption = req.body.caption;

    if (phone == undefined || image == undefined) {
        res.send({status:"error", message: 'please enter a valid phone and image'})
    } else {
        if (base64regex.test(image)) {          
            let media = new MessageMedia('image/png', image);
            console.log(phone), console.log(caption)
            try {
               client.sendMessage(phone, media, { caption: caption || '' })
                .then((resp) => {
                    console.log(resp)
                    if (resp.id.fromMe) {
                        res.send({
                            status: 'success',
                            message: `MediaMessage successfully sent to ${phone}`
                        })
                    }
                    console.log('success')
                })
            } catch (err) {
                res.status(500).json({
                    status: false,
                    error: err.message
                })
            }       
        } else if (vuri.isWebUri(image)) {
            if (!fs.existsSync('./temp')) {
                await fs.mkdirSync('./temp')
            }
            var path = './temp/' + image.split("/").slice(-1)[0]
            mediaDownloader(image, path, () => {
                let media = MessageMedia.fromFilePath(path);

                client.sendMessage(phone, media, { caption: caption || '' }).then((resp) => {
                    if (resp.id.fromMe) {
                        res.send({
                            status: 'success',
                            message: `MediaMessage successfully sent to ${phone}`
                        })
                        fs.unlinkSync(path)
                    }
                })
            })
        } else {
            console.log('error');
            res.send({status:'error', message: 'Invalid Url/Base64 Encoded Media'})
        }
    }
})


router.post('/sendpdf/:phone', async (req,res) => {
    var base64regex = /^([0-9a-zA-Z+/]{4})*(([0-9a-zA-Z+/]{2}==)|([0-9a-zA-Z+/]{3}=))?$/;

    let phone = phoneFormatter(req.params.phone);
    let pdf = req.body.image;

    if (phone == undefined || pdf == undefined) {
        res.send({ status: "error", message: "please enter valid phone and base64/url of pdf" })
    } else {
        if (base64regex.test(pdf)) {
            let media = new MessageMedia('application/pdf', pdf);
            try {
                client.sendMessage(phone, media).then((response) => {
                    if (response.id.fromMe) {
                        res.send({ status: 'success', message: `MediaMessage successfully sent to ${phone}` })
                    }
                });
            } catch (err) {
                res.status(500).json({
                    status: false,
                    error: err.message
                })
            }
            
        } else if (vuri.isWebUri(pdf)) {
            if (!fs.existsSync('./temp')) {
                await fs.mkdirSync('./temp');
            }

            var path = './temp/' + pdf.split("/").slice(-1)[0]
            mediadownloader(pdf, path, () => {
                let media = MessageMedia.fromFilePath(path);
                client.sendMessage(`${phone}@c.us`, media).then((response) => {
                    if (response.id.fromMe) {
                        res.send({ status: 'success', message: `MediaMessage successfully sent to ${phone}` })
                        fs.unlinkSync(path)
                    }
                });
            })
        } else {
            res.send({ status: 'error', message: 'Invalid URL/Base64 Encoded Media' })
        }
    }
});

router.post('/sendlocation/:phone', async (req, res) => {
    let phone = phoneFormatter(req.params.phone);
    let latitude = req.body.latitude;
    let longitude = req.body.longitude;
    let desc = req.body.description;

    if (phone == undefined || latitude == undefined || longitude == undefined) { 
        res.send({ status: "error", message: "please enter valid phone, latitude and longitude" })
    } else {
        let loc = new Location(latitude, longitude, desc || "");
        client.sendMessage(`${phone}@c.us`, loc).then((response)=>{
            if (response.id.fromMe) {
                res.send({ status: 'success', message: `MediaMessage successfully sent to ${phone}` })
            }
        });
    }
});

router.get('/getchatbyid/:phone', async (req, res) => {
    let phone = phoneFormatter(req.params.phone);
    if (phone == undefined) {
        res.send({status:"error",message:'please provide a valid phone number'})
    } else {
        client.getChatById(phone).then(chat => {
           chat.fetchMessages().then(data => {
                res.send({
                    status: 'success',
                    message: data
                })
            })
        }).catch(() => {
            console.error("getChatError")
            res.send({status: "error",message:"getChatError"})
        })
    }
})

router.get('/chats', async (req, res) => {
   await client.getChats().then((chats) => {
        console.log(chats);
        res.send({ status: "success", message: chats });
    }).catch((err) => {
        res.send({ status: "error",message: "getchatserror" })
    })
})

module.exports = router