const router = require('express').Router()
const { MessageMedia, Location } = require('whatsapp-web.js')
const { body, validationResult } = require('express-validator');
const request = require('request')
const vuri = require('valid-url')
const fs = require('fs')

const mediaDownloader = (url, path, callback) => {
    request.head(url, (err, res, body) => {
        request(url).pipe(fs.createWriteStream(path))
        .on('close',callback)
    })
}

router.post('/creategroup/:name', async (req, res) => {
    let groupname = req.params.name;
    let contacts = req.body.contacts;
    var users = [];

    if (groupname == undefined || contacts == undefined) {
        res.send({ status: 'error', message: 'Please Fill required fields'})
    } else {
        
    }
})

router.post('/sendmessage/:chatname', async (req, res) => {
    let chatname = req.params.chatname;
    let message = req.body.message;

    if (chatname == undefined || message == undefined) {
        res.send({status:'error',message: 'please enter a valid chatname and phone number'})
    } else {
        Client.getChats().then((data) => {
            data.forEach(chat => {
                if (chat.id.server === "g.us" && chat.name === chatname) {
                    Client.sendMessage(chat.id._serialized, message)
                        .then((response) => {
                            if (response.id.fromMe) {
                                res.send({status:'success',message: `Message successfully send to ${chatname}`})
                            }
                        })
                }
            });
        })
    }
})

const findGroupByName = async function (name) {
    const group = await client.getChats().then(chats => {
        return chats.find(chat => {
            chat.isGroup && chat.name.toLowerCase() == name.toLowerCase()
        })
    })
    return group;
}

router.post('/send-group-message', [
    body('id').custom((val, { req }) => {
        if (!val && !req.body.name) {
            throw new Error('Invalid Value, you can use `id` or `name`')
        }
        return true;
    }),
    body('message').notEmpty()
], async (req, res) => {
    let chatId = req.body.id;
    const groupName = req.body.name;
    const message = req.body.message;

    //find group by name
    if (!chatId) {
        const group = await findGroupByName(groupName);
        if (!group) {
            res.send({
                status: false,
                message: 'No Group found with name: ' + groupName
            });
        }
        chatId = group.id._serialized;
    }

    client.sendMessage(chatId, message).then(resp => {
        res.send({
            status: true,
            response: resp
        })
    }).catch(err => {
        res.status(500).json({
            status: false,
            response: err
        })
    })
})

module.exports = router