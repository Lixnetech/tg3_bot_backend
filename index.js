const express = require("express");
const fs = require("fs");
const qrCode = require("qrcode-terminal");
const http = require("http");
const socketIo = require("socket.io");
const { Client } = require("whatsapp-web.js");
var cors = require("cors");
const axios = require("axios");

var redis = require("redis");

const config = require("./config.json");
const SESSION_FILE = "./session.json";

const port = process.env.PORT || 4000;

//load session data if it had previously been saved
let sessionCfg;
if (fs.existsSync(SESSION_FILE)) {
	sessionCfg = require(SESSION_FILE);
}

global.client = new Client({
	puppeteer: {
		headless: true,
		args: [
			"--no-sandbox",
			"--disable-setuid-sandbox",
			"--unhandled-rejections=strict",
			"--disable-accelerated-2d-canvas",
			"--no-first-run",
			"--no-zygote",
		],
	},
	restartOnAuthFail: true,
	clientId: "tg3",
});

process.title = "tg3-bot";
global.authenticated = false;

const app = express();
app.use(cors());

const server = http.createServer(app);
global.io = socketIo(server, {
	cors: {
		origin: "http://127.0.0.1:8000",
		methods: ["GET", "POST"],
	},
});

app.use(express.json({ limit: "50mb" }));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

client.initialize();

io.on("connection", function (socket) {
	socket.emit("message", "Connecting...");

	var redisClient = redis.createClient();

	redisClient.subscribe("text");

	redisClient.on("text", function (channel, data) {
		var data = JSON.parse(data);
		console.log("new text");
		io.emit(channel, data);
		// if (data.message != '') {

		// }
	});

	client.on("qr", (qr) => {
		//this will not be fired if a session is specified
		qrCode.generate(qr, { small: true });
		try {
			fs.writeFileSync("./routes/last.qr", qr);
		} catch (err) {}
	});

	client.on("ready", async (data) => {
		console.log("READY");
		socket.emit("ready", "Whatsapp is Ready!");
		socket.emit("message", "Whatsapp is Ready!");
	});

	client.on("authenticated", () => {
		console.log("AUTHENTICATED");
		socket.emit("authenticated", "Authentication Successful");
		socket.emit("message", "Authentication Successful");
	});

	client.on("auth_failure", (msg) => {
		//fired if session restore was unsuccessful
		console.error("AUTH FAILURE", msg);
		socket.emit("message", "Authentication Failed, retrying...");
		process.exit();
	});

	client.on("disconnected", (reason) => {
		console.log("Client was logged out", reason);
		socket.emit("message", "Whatsapp Was Disconnected!");

		try {
			fs.unlinkSync("route/last.qr");
		} catch (err) {}
		client.destroy();
		client.initialize();
	});
});

client.on("message", async (msg) => {
	console.log("message", msg);
	if (config.webhook.enabled) {
		if (msg.author == undefined) {
			io.emit("message", msg);
			axios.post(config.webhook.path, { msg });
		}
	}
	if (msg.body.startsWith("Number ")) {
		let num = msg.body.split(" ")[1];
		var ans = Math.floor(Math.random() * Number(num));
		console.log(ans);
		msg.reply(`The answer is ${ans}. Thanks For playing`);
	} else if (msg.body.toLowerCase() == "hello") {
		msg.getChat();
		msg.reply(
			`Hi i'm TG3, I dont know the response to this yet but i'm learning`
		);
	}
});

client.on("message_ack", (msg, ack) => {
	if (ack == 3) {
		io.emit("message", msg);
		if (config.webhook.enabled) {
			axios.post(config.webhook.path, { ack: "test", msg });
		}
	}
});

client.on("message_create", async (msg) => {
	// Fired on all message creations, including your own
	if (msg.fromMe) {
		console.log("create", msg);
		if (config.webhook.enabled) {
			io.emit("message", msg);
			axios.post(config.webhook.path, { fromMe: "yes", msg });
		}
	}
});

client.on("message_revoke_everyone", async (after, before) => {
	// Fired whenever a message is deleted by anyone (including you)
	console.log(after); // message after it was deleted.
	if (before) {
		console.log(before); // message before it was deleted.
	}
});

client.on("message_revoke_me", async (msg) => {
	// Fired whenever a message is only deleted in your own view.
	console.log("deleted", msg.body); // message before it was deleted.
	io.emit("deleted", msg.body);
});

client.on("change_battery", (batteryInfo) => {
	// Battery percentage for attached device has changed
	const { battery, plugged } = batteryInfo;
	console.log(`Battery: ${battery}% - Charging? ${plugged}`);
});

client.on("change_state", (state) => {
	console.log("CHANGE STATE", state);
	io.emit("STATE CHANGED", state);
});

const authRoute = require("./routes/auth");
const chatsRoute = require("./routes/chat");
const contactsRoute = require("./routes/contact");
const groupsRoute = require("./routes/group");

app.use(function (req, res, next) {
	console.log(req.method + " : " + req.path);
	next();
});

app.use("/auth", authRoute);
app.use("/chat", chatsRoute);
app.use("/contact", contactsRoute);
app.use("/group", groupsRoute);

server.listen(port, () => {
	console.log("server running on http://localhost:" + port);
});
